<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script>
        function showpass(){
            if(document.getElementById("password1").type == 'text'){
                document.getElementById("password1").type = 'password';
                document.getElementById("password2").type = 'password';
            } else {
                document.getElementById("password1").type = 'text';
                document.getElementById("password2").type = 'text';
            }
            
        }    
        </script>
    </head>
<body onload="showpass();">
    <form method="post" action="php/signup.php">
    <table cellpadding="5px">
    <tr>
        <td><span id="errors" style="color: red;"><?php require_once("php/error.php"); ?></span></td>
        </tr>
    <tr>
        <td>First Name</td>
        <td><input type="text" name="fname"></td>
        </tr>
    <tr>
        <td>Last Name</td>
        <td><input type="text" name="lname"></td>
        </tr>
    <tr>
        <td>Email</td>
        <td><input type="email" name="email"></td>
        </tr>
    <tr>
        <td>Password</td>
        <td><input type="text" id="password1" name="password1"></td>
        <td><input tabindex="-1" type="checkbox" onclick="showpass();"></td>
        </tr>
    <tr>
        <td>Verify Password</td>
        <td><input type="password" id="password2" name="password2"></td>
        </tr>
    <tr>
        <td></td>
        <td><input type="submit"></td>
        </tr>
    </table>
        </form>
</body>
</html>
